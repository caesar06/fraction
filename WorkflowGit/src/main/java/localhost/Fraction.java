package localhost;

/**
 *
 * @name : Fraction
 * This class represents a Fraction
 *
 *
 */
public final class Fraction {


    /**
     * Fraction nominator
     */
    private double numerateur = 0;
    /**
     * Fraction denomniator
     */
    private double denominateur = 1;

    /**
     * Fraction representing the constant 0
     */
    public static final Fraction ZERO = new Fraction(0,0);

    /**
     * Fraction representing the constant 1
     */
    public static final Fraction ONE = new Fraction(1,1);


    /**
     * Constructor taking nominator and denominator arguments.
     * @param numerateur
     * @param denominateur
     */
    public Fraction(int numerateur, int denominateur)
    {
        try {
            if (denominateur != 0) {
                this.numerateur = numerateur;
                this.denominateur = denominateur;
            } else {
                throw new IllegalArgumentException();
            }
        }
        catch(Exception e){

            System.out.println("Divison by 0 undefined, fraction constructor failed");
        }


    }

    /**
     *  Constructor to initialize using nominator only, denominator is set to 0
     *
     * @param numerateur
     */
    public Fraction(int numerateur){

        this.numerateur = numerateur;

        this.denominateur = 1;
    }

    /**
     * Default constructor with no parameters, fraction is set to 0/1
     */
    public Fraction() {}


    /**
     *
     */
    public String toString(){
        if ( this.denominateur != 1 ) {
            return this.numerateur + "\\" + this.denominateur;
        }
        return this.numerateur + "";
    }



    public double getNumeraeur()
    {
        return numerateur;
    }

    public double getDenominateur()
    {
        return denominateur;
    }
    public double consultation()
    {
        return this.numerateur/this.denominateur;
    }



}
